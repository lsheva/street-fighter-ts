var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
var logger = require("morgan");
const expressValidator = require("express-validator");
const cors = require("cors");

var indexRouter = require("./routes/index");
var userRouter = require("./routes/user");

var app = express();

app.use(cors({ credentials: true, origin: true }));
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(expressValidator());
app.use(cookieParser());

app.use("/", indexRouter);
app.use("/user", userRouter);

module.exports = app;
