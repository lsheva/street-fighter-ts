const fs = require("fs");
const dataPath = "../data/userlist.json";
const data = require(dataPath);

const { body } = require("express-validator/check");

// збереження даних в json file
const saveData = () => {
  const json = JSON.stringify(data, null, 2);
  const path = __dirname + "/" + dataPath;

  // перетворили writeFile у проміс
  return new Promise((resolve, reject) => {
    fs.writeFile(path, json, err => {
      if (err) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

// Validation handler

const validationHandler = next => result => {
  if (result.isEmpty()) return;
  if (!next)
    throw new Error(
      result
        .array()
        .map(i => `'${i.param}' - ${i.msg}`)
        .join(" ")
    );
  else
    return next(
      new Error(
        result
          .array()
          .map(i => `'${i.param}' - ${i.msg}`)
          .join("")
      )
    );
};

exports.validate = method => {
  switch (method) {
    case "user": {
      return [
        body("name", "Name doesn't exists").exists(),
        body("health", "Health doesn't exists").exists(),
        body("health", "Health must be from 1 to 100").isInt({
          min: 1,
          max: 100
        }),
        body("attack", "Attack doesn't exists").exists(),
        body("attack", "Attack must be from 1 to 100").isInt({
          min: 1,
          max: 100
        }),
        body("defense", "Defense doesn't exists").exists(),
        body("defense", "Defense must be from 1 to 100").isInt({
          min: 1,
          max: 100
        }),
        body("source", "Source doesn't exists").exists(),
        body("source", "Source must be a valid url").isURL()
      ];
    }
  }
};

// отримання масиву всіх користувачів
exports.getAll = (req, res) => {
  res.send(data);
};

// отримання одного користувача по ID
exports.getOne = (req, res) => {
  const user = data.find(e => e._id == req.params.id);
  if (!user) {
    return res.status(404).send({
      message: `User not found with id ${req.params.id}`
    });
  }
  res.send(user);
};

// створення користувача за даними з тіла запиту
exports.create = (req, res, next) => {
  req
    .getValidationResult()
    .then(validationHandler())
    .then(() => {
      const { name, health, attack, defense, source } = req.body;

      // пошук найбільшого цифрового id для того, щоб
      // присвоїти новому елементу id+1
      // можливо є більш елегантне рішення?
      const lastId = data.reduce((max, cur) => {
        const curId = Number(cur._id);
        if (curId) {
          return Math.max(curId, max);
        }
        return max;
      }, 0);

      const user = { _id: lastId + 1, name, health, attack, defense, source };
      data.push(user);

      // збереження
      saveData()
        .then(() => {
          console.log("success");
          res.send(user);
        })
        .catch(err => {
          console.log("error");
          res.status(500).send({
            message: err.message || "Something wrong while creating user"
          });
        });
    })
    .catch(next);
};

// оновлення користувача за даними з тіла запиту

exports.update = (req, res, next) => {
  const user = data.find(e => e._id == req.params.id);
  if (!user) {
    return res.status(404).send({
      message: `User not found with id ${req.params.id}`
    });
  }

  req
    .getValidationResult()
    .then(validationHandler())
    .then(() => {
      const { name, health, attack, defense, source } = req.body;
      user.name = name;
      user.health = health;
      user.attack = attack;
      user.defense = defense;
      user.source = source;

      // збереження
      saveData()
        .then(() => {
          console.log("success");
          res.send(user);
        })
        .catch(err => {
          console.log("error");
          res.status(500).send({
            message: err.message || "Something wrong while updating user"
          });
        });
    })
    .catch(next);
};

exports.delete = (req, res) => {
  const arrayId = data.findIndex(e => e._id == req.params.id);
  if (arrayId === "false") {
    // important as it could be zero which converts to false
    return res.status(404).send({
      message: `User not found with id ${req.params.id}`
    });
  }

  data.splice(arrayId, 1);

  saveData()
    .then(() => {
      console.log("success");
      res.send({ message: "User deleted successfully!" });
    })
    .catch(err => {
      console.log("error");
      res.status(500).send({
        message: err.message || "Something wrong while deleting user"
      });
    });
};
