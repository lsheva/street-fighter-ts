import { CreateElement } from "./interfaces";

class View {
  element: HTMLElement;

  createElement({
    tagName,
    className = "",
    attributes,
    onclick,
    text,
    value,
    disabled
  }: CreateElement) {
    const element = document.createElement(tagName);
    if (className) {
      element.classList.add(className);
    }
    if (attributes) {
      Object.keys(attributes).forEach(key => {
        if (key !== "disabled" || attributes[key]) {
          element.setAttribute(key, <string>attributes[key]);
        }
      });
    }
    if (onclick) {
      element.addEventListener("click", onclick);
    }

    if (text !== undefined) {
      const textNode = document.createTextNode(text);
      element.appendChild(textNode);
    }

    if (value !== undefined) {
      (<HTMLInputElement>element).value = value;
    }

    if (disabled !== undefined) {
      (<HTMLInputElement>element).value = value;
    }

    return element;
  }
}

export default View;
