import View from "./view";
import App from "./app";
import { Fighter, FormFields, HandleClick } from "./interfaces";

class ModalView extends View {
  fields: Array<HTMLElement>;

  constructor(
    modal: Fighter,
    handleClick: HandleClick,
    editableFieldsArray: Array<String>
  ) {
    super();

    this.createModal(modal, handleClick, editableFieldsArray);
  }

  onSubmit(e: Event, handleClick: HandleClick) {
    e.preventDefault();
    const data = this.fields.reduce((acc: FormFields, cur) => {
      const input = cur.querySelector("input");
      acc[input.getAttribute("name")] = input.value;
      return acc;
    }, {});
    handleClick(data);
    this.element.remove();
  }

  createModal(
    modal: Fighter,
    handleClick: HandleClick,
    editableFieldsArray: Array<String>
  ) {
    this.fields = Object.keys(modal).map(key => {
      if (hasKey(modal, key)) {
        return this.createField(
          key,
          <string>modal[key],
          editableFieldsArray.includes(key)
        );
      } else {
        return null;
      }
    });

    this.element = this.createElement({
      tagName: "div",
      className: "modal-wrapper"
    });

    const form = this.createElement({
      tagName: "form",
      className: "modal"
    });

    const submit = this.createElement({
      tagName: "button",
      className: "submitButton",
      onclick: e => this.onSubmit(e, handleClick),
      text: "Save"
    });

    form.append(...this.fields, submit);
    this.element.append(form);
    console.log(this.fields);
    App.rootElement.append(this.element);

    // Handle click outside modal
    this.element.onclick = e => {
      if (e.target == this.element && e.target !== form) {
        this.element.remove();
      }
    };
  }

  createField(name: string, value: string, editable = false): HTMLElement {
    const field = this.createElement({
      tagName: "div",
      className: "field"
    });

    const label = this.createElement({
      tagName: "label",
      text: name
    });

    const input = this.createElement({
      tagName: "input",
      attributes: {
        type: "text",
        name,
        value,
        disabled: !editable
      }
    });

    label.append(input);
    field.append(label);

    return field;
  }

  createSubmit(name: string, onSubmitHandler: HandleClick) {
    const submit = document.createElement("button");
    submit.innerHTML = name;
    submit.onclick = e => {
      e.preventDefault();
      const data = this.fields.reduce((acc: FormFields, cur) => {
        const input = cur.querySelector("input");
        acc[input.getAttribute("name")] = input.value;
        return acc;
      }, {});
      onSubmitHandler(data);
    };
    return submit;
  }
}

function hasKey<O>(obj: O, key: keyof any): key is keyof O {
  return key in obj;
}

export default ModalView;
