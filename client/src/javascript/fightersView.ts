import View from "./view";
import FighterView from "./fighterView";
import ModalView from "./modalView";
import BattleView from "./battleView";
import { fighterService } from "./services/fightersService";
import App from "./app";
import {
  Fighter,
  SelectedPlayer,
  HandleEditClick,
  FormFields
} from "./interfaces";

class FightersView extends View {
  private player1id: string;
  private player2id: string;
  private startButton: HTMLElement;
  battleView: BattleView;

  handleEditClick: HandleEditClick;
  fighterSelect: (fighter: Fighter) => SelectedPlayer;

  constructor(fighters: Array<Fighter>) {
    super();

    this.handleEditClick = this.handleFighterEditClick.bind(this);
    this.fighterSelect = this.handleFighterSelect.bind(this);

    this.createFighters(fighters);
    this.player1id = null;
    this.player2id = null;
    this.createStartButton();
  }

  fightersDetailsMap = new Map();

  createFighters(fighters: Array<Fighter>) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(
        fighter,
        this.handleEditClick,
        this.fighterSelect
      );
      return fighterView.element;
    });

    this.element = this.createElement({
      tagName: "div",
      className: "fighters"
    });
    this.element.append(...fighterElements);
  }

  // loads fighter info to map
  async updateFighterMap(fighterId: string) {
    const fighterDetails = await fighterService.getFighterDetails(fighterId);
    this.fightersDetailsMap.set(fighterId, fighterDetails);
  }

  async loadFighterDetails(fighterId: string) {
    // check if fighter info is already loaded into map
    if (!this.fightersDetailsMap.get(fighterId)) {
      await this.updateFighterMap(fighterId);
    }
  }

  async handleFighterEditClick(event: Event, fighter: Fighter) {
    event.stopPropagation();
    console.log("edit");
    await this.loadFighterDetails(fighter._id);

    // show modal with fighter info
    new ModalView(
      this.fightersDetailsMap.get(fighter._id),
      async (data: FormFields) => {
        await fighterService.updateFighterDetails(data._id, data);
        this.updateFighterMap(fighter._id);
      },
      ["health", "attack"]
    );
  }

  fighterSelectLogic(fighterId: string) {
    if (fighterId == this.player1id) {
      this.player1id = null;
      return { player: "player1", selected: false };
    } else if (fighterId == this.player2id) {
      this.player2id = null;
      return { player: "player2", selected: false };
    } else {
      if (this.player1id && this.player2id) {
        // do nothing
        return { player: false, selected: false };
      } else if (this.player1id) {
        this.player2id = fighterId;
        return { player: "player2", selected: true };
      } else if (this.player2id) {
        this.player1id = fighterId;
        return { player: "player1", selected: true };
      } else {
        this.player1id = fighterId;
        return { player: "player1", selected: true };
      }
    }
  }

  handleFighterSelect(fighter: Fighter) {
    const ret = this.fighterSelectLogic(fighter._id);
    // returns true if became selected, false if not

    if (this.player1id && this.player2id) {
      this.startButton.style.display = "block";
    } else {
      this.startButton.style.display = "none";
    }
    return ret;
  }

  createStartButton() {
    this.startButton = this.createElement({
      tagName: "button",
      className: "start-button",
      text: "Start Game",
      onclick: async () => {
        // rewrite to run paralelly
        await this.loadFighterDetails(this.player1id);
        await this.loadFighterDetails(this.player2id);

        this.battleView = new BattleView(
          this.fightersDetailsMap.get(this.player1id),
          this.fightersDetailsMap.get(this.player2id)
        );
      }
    });

    App.rootElement.append(this.startButton);
  }
}

export default FightersView;
