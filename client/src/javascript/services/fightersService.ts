import { callApi } from "../helpers/apiHelper";
import { Fighter } from "../interfaces";

class FighterService {
  async getFighters(): Promise<Array<Fighter>> {
    try {
      const endpoint = "user";
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(_id: string): Promise<Fighter> {
    try {
      const endpoint = `user/${_id}`;
      const apiResult = await callApi(endpoint, "GET");

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async updateFighterDetails(_id: string, data: object): Promise<void> {
    try {
      const endpoint = `user/${_id}`;
      const apiResult = await callApi(endpoint, "PUT", data);

      return apiResult;
    } catch (error) {
      throw error;
    }
  }
}

const fighterService = new FighterService();

export { fighterService };
