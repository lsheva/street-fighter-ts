import View from "./view";

class HealthBar extends View {
  health: number;
  position: "left" | "right";
  healthElement: HTMLElement;

  constructor(position: "left" | "right") {
    super();
    this.health = 100;
    this.position = position;
    this.createHealthBar();
  }

  createHealthBar() {
    let className;
    if (this.position == "right") {
      className = "health-bar-right";
    } else {
      className = "health-bar-left";
    }

    this.healthElement = this.createElement({
      tagName: "div",
      className: "health-element"
    });

    this.element = this.createElement({
      tagName: "div",
      className: className
    });

    this.element.append(this.healthElement);

    this.updateHealth();
  }

  updateHealth(health?: number) {
    if (health) {
      this.health = health;
    }
    this.healthElement.style.width = this.health + "%";
  }
}

export default HealthBar;
