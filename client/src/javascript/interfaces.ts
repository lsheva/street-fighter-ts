import HealthBar from "./healthBarView";

export interface Fighter {
  _id: string;
  name: string;
  source: string;
  health?: number;
  attack?: number;
  defense?: number;
}

export interface SelectedPlayer {
  player: "player1" | "player2" | false;
  selected: boolean;
}

export interface CreateElement {
  tagName: string;
  className?: string;
  attributes?: { [key: string]: string | boolean };
  onclick?: (event: Event) => Promise<void> | void;
  text?: string;
  value?: string;
  disabled?: boolean;
}

export interface HandleEditClick {
  (event: Event, fighter: Fighter): Promise<void>;
}

export interface FormFields {
  [key: string]: string;
}

export interface HandleClick {
  (data: FormFields): Promise<void>;
}

export interface Player {
  details: Fighter;
  status: 0 | 1 | -1;
  health: number;
  element: HTMLElement;
  healthBar?: HealthBar;
}
